import { BaseEditor, BaseText } from "slate";
import { ReactEditor } from "slate-react";

type Paragraph = { type: "paragraph"; children: BaseText[] };

type Code = { type: "code"; children: BaseText[] };

type Image = { type: "image"; src: string; alt: string; children: never[] };

declare module "slate" {
  interface CustomTypes {
    Editor: BaseEditor & ReactEditor;
    Element: Paragraph | Code | Image;
  }
}
