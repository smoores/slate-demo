import { useEffect, useState } from "react";
import {
  createEditor,
  Editor,
  Transforms,
  Element,
  Node,
  Operation,
} from "slate";
import { withReact, Slate, Editable, RenderElementProps } from "slate-react";
import { CodeElement } from "./CodeElement";
import { clientId, subscribeToOperations } from "../firestore";
import { FirestoreOperation } from "../FirestoreOperation";
import { useAppSelector, useAppDispatch } from "../hooks";
import { ImageElement } from "./ImageElement";
import { ParagraphElement } from "./ParagraphElement";
import {
  applyRemoteOperations,
  confirmOperations,
  editorUpdated,
  writeOperations,
} from "../store/slices/editor";

function renderElement({ element, attributes, children }: RenderElementProps) {
  switch (element.type) {
    case "paragraph": {
      return (
        <ParagraphElement element={element} attributes={attributes}>
          {children}
        </ParagraphElement>
      );
    }
    case "code": {
      return (
        <CodeElement element={element} attributes={attributes}>
          {children}
        </CodeElement>
      );
    }
    case "image": {
      return (
        <ImageElement element={element} attributes={attributes}>
          {children}
        </ImageElement>
      );
    }
    default: {
      return <div {...attributes}>{children}</div>;
    }
  }
}

function withImages(editor: Editor) {
  const { isVoid } = editor;

  editor.isVoid = (element) =>
    element.type === "image" ? true : isVoid(element);

  return editor;
}

const editor = withReact(withImages(createEditor()));

export function SlateEditor() {
  const editorValue = useAppSelector((state) => state.editor.value);
  const [editorLoaded, setEditorLoaded] = useState(editorValue !== null);
  const dispatch = useAppDispatch();

  useEffect(() => {
    setEditorLoaded(editorValue !== null);
  }, [editorValue]);

  useEffect(() => {
    if (editorLoaded) {
      subscribeToOperations((operations) => {
        const partitioned = operations.reduce((acc, operation) => {
          if (!acc.length) {
            acc.push([operation]);
            return acc;
          }
          if (acc[acc.length - 1][0].clientId !== operation.clientId) {
            acc.push([operation]);
            return acc;
          }
          acc[acc.length - 1].push(operation);
          return acc;
        }, [] as FirestoreOperation[][]);
        partitioned.forEach((partition) => {
          if (partition[0].clientId === clientId) {
            dispatch(
              confirmOperations(
                partition.map((operation) => operation.operationId!)
              )
            );
          } else {
            dispatch(applyRemoteOperations({ editor, operations: partition }));
          }
        });
      });
    }
  }, [dispatch, editorLoaded]);

  return (
    editorValue && (
      <Slate
        editor={editor}
        value={editorValue}
        onChange={(newValue) => {
          const { operationsToWrite, remoteOperations } = (
            editor.operations as FirestoreOperation[]
          ).reduce(
            (acc, operation) => {
              if (
                (!("clientId" in operation) ||
                  (operation.clientId === clientId && !operation.rebased)) &&
                operation.type !== "set_selection"
              ) {
                acc.operationsToWrite.push(operation);
              } else if (
                "clientId" in operation &&
                operation.clientId !== clientId
              ) {
                acc.remoteOperations.push(operation);
              }
              return acc;
            },
            {
              operationsToWrite: [] as Operation[],
              remoteOperations: [] as FirestoreOperation[],
            }
          );
          if (remoteOperations.length) {
            const latestVersion = Math.max(
              ...remoteOperations.map((operation) => operation.operationId!)
            );
            dispatch(
              editorUpdated({ value: newValue, version: latestVersion })
            );
          }
          if (operationsToWrite.length) {
            dispatch(
              writeOperations({ newValue, operations: operationsToWrite })
            );
          }
        }}
      >
        <Editable
          renderElement={renderElement}
          onKeyDown={(event) => {
            if (event.key === "`" && event.ctrlKey) {
              event.preventDefault();
              const [match] = Editor.nodes(editor, {
                match: (node) => (node as Element).type === "code",
              });
              Transforms.setNodes(
                editor,
                { type: match ? "paragraph" : "code" },
                { match: (n) => Editor.isBlock(editor, n) }
              );
            }
            if (event.key === "i" && event.ctrlKey) {
              event.preventDefault();
              const [match] = Editor.nodes(editor, {
                match: (node) => (node as Element).type === "paragraph",
              });
              Transforms.setNodes(
                editor,
                {
                  type: "image",
                  src: Node.string(match[0]),
                  alt: Node.string(match[0]),
                  children: [],
                },
                {
                  match: (node) => (node as Element).type === "paragraph",
                }
              );
            }
          }}
        />
      </Slate>
    )
  );
}
