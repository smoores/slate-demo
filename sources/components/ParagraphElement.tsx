import { RenderElementProps } from "slate-react";

export function ParagraphElement({ attributes, children }: RenderElementProps) {
  return <p {...attributes}>{children}</p>;
}
