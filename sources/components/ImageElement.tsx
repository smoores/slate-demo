import { BaseElement } from "slate";
import { RenderElementProps } from "slate-react";

type Props = RenderElementProps & {
  element: {
    type: "image";
    src: string;
    alt: string;
  } & BaseElement;
};

export function ImageElement({ element, attributes }: Props) {
  return <img src={element.src} alt={element.alt} {...attributes} />;
}
