import { RenderElementProps } from "slate-react";

export function CodeElement({ attributes, children }: RenderElementProps) {
  return (
    <pre {...attributes}>
      <code>{children}</code>
    </pre>
  );
}
