import ReactDOM from "react-dom";
import { StrictMode, useEffect, useState } from "react";
import { Provider, useDispatch } from "react-redux";
import { store } from "./store";
import { SlateEditor } from "./components/SlateEditor";
import {
  createNewEditor,
  getEditorId,
  getEditorValue,
  getEditorVersion,
} from "./firestore";
import { editorLoaded } from "./store/slices/editor";

function App() {
  const [editorId, setEditorId] = useState(getEditorId);
  const dispatch = useDispatch();
  useEffect(() => {
    async function updateEditorValue() {
      if (!editorId) return;
      const editorValue = await getEditorValue();
      const editorVersion = await getEditorVersion();
      dispatch(editorLoaded({ value: editorValue, version: editorVersion }));
    }
    updateEditorValue();
  }, [dispatch, editorId]);

  return (
    <StrictMode>
      <header>
        <h1>Slate Demo</h1>
      </header>
      {editorId ? (
        <SlateEditor />
      ) : (
        <button
          type="button"
          onClick={async () => {
            const newEditorId = await createNewEditor();
            setEditorId(newEditorId);
            window.history.pushState({}, "", `/${newEditorId}`);
          }}
        >
          Create Editor
        </button>
      )}
    </StrictMode>
  );
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
