import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  addDoc,
  getDoc,
  doc,
  query,
  orderBy,
  limit,
  getDocs,
  onSnapshot,
  CollectionReference,
  runTransaction,
  serverTimestamp,
} from "firebase/firestore";
import { Descendant } from "slate";
import { v4 as uuidv4 } from "uuid";
import { FirestoreOperation } from "./FirestoreOperation";
import { store } from "./store";

const firebaseConfig = {
  apiKey: "AIzaSyBwdu2Cn5srxu0Xrz_WpyfuKERryfwS6h4",
  authDomain: "forward-curve-332122.firebaseapp.com",
  projectId: "forward-curve-332122",
  storageBucket: "forward-curve-332122.appspot.com",
  messagingSenderId: "1000075252439",
  appId: "1:1000075252439:web:76b78cb294f1b194755692",
};

export const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);

export function getEditorId() {
  return window.location.pathname.slice(1);
}

export const clientId = uuidv4();

export async function createNewEditor() {
  const topCollectionRef = collection(db, "slate-docs");
  const newEditor = await addDoc(topCollectionRef, {
    value: [
      {
        type: "paragraph",
        children: [{ text: "A paragraph with text" }],
      },
    ],
  });
  return newEditor.id;
}

let taskQueue = Promise.resolve();

class OperationIdConflictError extends Error {}

export async function writeOperations() {
  const operationsRef = collection(
    db,
    "slate-docs",
    getEditorId(),
    "operations"
  );
  taskQueue = taskQueue
    .then(async () =>
      runTransaction(db, async (transaction) => {
        const operationsToWrite = store.getState().editor.unconfirmed;
        if (!operationsToWrite.length) return;
        const existingDocs = await Promise.all(
          operationsToWrite.map((operation) => {
            const docRef = doc(
              operationsRef,
              operation.operationId!.toString()
            );
            return transaction.get(docRef);
          })
        );

        // eslint-disable-next-line no-restricted-syntax
        for (const existingDoc of existingDocs) {
          if (existingDoc.exists()) {
            throw new OperationIdConflictError(
              `tried to write ${JSON.stringify(
                operationsToWrite,
                null,
                2
              )}, but operation already exists with id ${existingDoc.id}`
            );
          }
        }

        // eslint-disable-next-line no-restricted-syntax
        for (const operation of operationsToWrite) {
          const docRef = doc(operationsRef, operation.operationId!.toString());
          transaction.set(docRef, {
            ...operation,
            createdAt: serverTimestamp(),
          });
        }
      })
    )
    .catch((e) => {
      if (!(e instanceof OperationIdConflictError)) {
        throw e;
      }
    });
}

export async function getEditorValue(): Promise<Descendant[]> {
  const editorRef = doc(db, "slate-docs", getEditorId());
  const editor = await getDoc(editorRef);
  return editor.get("value");
}

export async function getEditorVersion(): Promise<number> {
  const operationsRef = collection(
    db,
    "slate-docs",
    getEditorId(),
    "operations"
  );
  const latestOperationQuery = query(
    operationsRef,
    orderBy("operationId", "desc"),
    limit(1)
  );
  const querySnapshot = await getDocs(latestOperationQuery);
  return querySnapshot.docs[0]?.get("operationId") ?? 0;
}

export function subscribeToOperations(
  callback: (operation: FirestoreOperation[]) => void
) {
  const operationsRef = collection(
    db,
    "slate-docs",
    getEditorId(),
    "operations"
  ) as CollectionReference<FirestoreOperation>;
  const latestOperationQuery = query<FirestoreOperation>(
    operationsRef,
    orderBy("operationId", "asc")
  );
  return onSnapshot(latestOperationQuery, (querySnapshot) => {
    callback(
      querySnapshot.docChanges().map((docChange) => docChange.doc.data())
    );
  });
}
