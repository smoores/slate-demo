import { Operation } from "slate";

export type FirestoreOperation = Operation & {
  operationId?: number;
  clientId?: string;
  rebased?: true;
};
