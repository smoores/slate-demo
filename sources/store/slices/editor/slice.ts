import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { Descendant, Operation } from "slate";
import { EditorState } from "./state";

const initialState: EditorState = {
  value: null,
  version: null,
  unconfirmed: [],
};

export const editorSlice = createSlice({
  name: "editor",
  initialState,
  reducers: {
    editorLoaded: (
      state,
      action: PayloadAction<{ version: number; value: Descendant[] }>
    ) => {
      state.value = action.payload.value;
      state.version = action.payload.version;
    },
    editorUpdated: (
      state,
      action: PayloadAction<{
        value: Descendant[];
        unconfirmed?: Operation[];
        version?: number;
      }>
    ) => {
      state.value = action.payload.value;
      state.unconfirmed.push(...(action.payload.unconfirmed ?? []));
      if (action.payload.version) {
        state.version = action.payload.version;
      }
    },
    operationsRebased: (state) => {
      state.unconfirmed = [];
    },
    operationsConfirmed: (state, action: PayloadAction<number[]>) => {
      const firstUnconfirmed = state.unconfirmed[0];
      if (
        !firstUnconfirmed ||
        firstUnconfirmed.operationId !== action.payload[0]
      )
        return;
      state.unconfirmed.splice(0, action.payload.length);
      state.version = state.version
        ? state.version + action.payload.length
        : state.version;
    },
  },
});

export const {
  editorUpdated,
  editorLoaded,
  operationsRebased,
  operationsConfirmed,
} = editorSlice.actions;

export const editorReducer = editorSlice.reducer;
