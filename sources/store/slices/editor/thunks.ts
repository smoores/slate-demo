import { createAsyncThunk } from "@reduxjs/toolkit";
import { Descendant, Editor, Operation, Path, Transforms } from "slate";
import { editorUpdated, operationsRebased, operationsConfirmed } from "./slice";
import {
  clientId,
  writeOperations as writeOperationsToFirestore,
} from "../../../firestore";

import { EditorState } from "./state";
import { FirestoreOperation } from "../../../FirestoreOperation";

export const applyRemoteOperations = createAsyncThunk<
  void,
  { operations: FirestoreOperation[]; editor: Editor }
>(
  "editor/applyRemoteOperation",
  ({ operations, editor }, { dispatch, getState }) => {
    const state = getState() as { editor: EditorState };
    const {
      editor: { unconfirmed },
    } = state;
    if (!unconfirmed.length) {
      editor.operations = operations;
      operations.forEach((operation) => {
        Transforms.transform(editor, operation);
      });
    } else {
      const newOperations = unconfirmed
        .slice()
        .reverse()
        .map((operation) => Operation.inverse(operation))
        .map((operation) => ({ ...operation, rebased: true })) as Operation[];
      newOperations.push(...operations);
      const mappedUnconfirmed = unconfirmed
        .map((operation) => {
          if (!("path" in operation)) {
            return operation;
          }
          const newPath = operations.reduce<Path | null>(
            (acc, op) => acc && Path.transform(acc, op),
            operation.path
          );
          if (!newPath) {
            return null;
          }
          return { ...operation, path: newPath };
        })
        .filter((operation) => operation !== null) as FirestoreOperation[];
      newOperations.push(...mappedUnconfirmed);
      editor.operations = newOperations;
      dispatch(operationsRebased());
      newOperations.forEach((operation) => {
        Transforms.transform(editor, operation);
      });
    }
    editor.onChange();
    editor.operations = [];
  }
);

export const writeOperations = createAsyncThunk<
  void,
  {
    operations: FirestoreOperation[];
    newValue: Descendant[];
  }
>(
  "editor/writeOperations",
  async ({ operations, newValue }, { dispatch, getState }) => {
    const state = getState() as { editor: EditorState };
    const {
      editor: { version, unconfirmed },
    } = state;
    if (version === null) return;
    const firestoreOperations = operations.map((operation, index) => ({
      ...operation,
      operationId: version + unconfirmed.length + index + 1,
      clientId,
    }));
    writeOperationsToFirestore();
    dispatch(
      editorUpdated({ value: newValue, unconfirmed: firestoreOperations })
    );
  }
);

export const confirmOperations = createAsyncThunk<void, number[]>(
  "editor/confirmOperations",
  async (operations, { dispatch, getState }) => {
    dispatch(operationsConfirmed(operations));
    const state = getState() as { editor: EditorState };
    const {
      editor: { unconfirmed },
    } = state;
    if (unconfirmed.length) {
      writeOperationsToFirestore();
    }
  }
);
