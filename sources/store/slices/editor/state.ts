import { Descendant } from "slate";
import { FirestoreOperation } from "../../../FirestoreOperation";

export interface EditorState {
  value: Descendant[] | null;
  version: number | null;
  unconfirmed: FirestoreOperation[];
}
